import requests
import bs4

URL = "https://blog.knoldus.com/setting-up-master-slave-machines-using-jenkins/"
response = requests.get(URL)
#print(response.text)
#print(response.status_code)

# f = open("D:\\python-selenium\\WebScraping\\response.txt", 'wb')
#
# for data in response.iter_content(10000):
#     f.write(data)
#
# f.close()

parsedData = bs4.BeautifulSoup(response.text)
allLinks = parsedData.select('input')
#print(len(allLinks))

for i in allLinks:
    #print(i.getText())
    #print(i.get('href'))
    print(i.attrs)